/*globals it */

const require_stringify = require('./index.js')

const browserify = require('browserify')
const str = require('string-to-stream')
const concat = require('concat-stream-promise')

const chai = require('chai')
chai.should()


;[
  {
    test_name: 'single string require',
    name2code: {
      mod: 'module.exports = "mod"',
    },
    entry_requires: ['mod'],
  },
  {
    test_name: 'multiple string requires',
    name2code: {
      mod0: 'module.exports = "mod0"',
      mod1: 'module.exports = "mod1"',
    },
    entry_requires: ['mod0', 'mod1'],
  },
  {
    test_name: 'nested string requires',
    name2code: {
      top: 'module.exports = require("bottom")',
      bottom: 'module.exports = "bottom"',
    },
    entry_requires: ['top'],
  },
].forEach(({test_name, name2code, entry_requires}) => {
  it(test_name, async function() {
    const entry = entry_requires
      .map(mod => `require("${mod}")`)
      .join('\n')

    const bundle = await browserify(str(entry))
      .plugin(require_stringify)
      .require_stringify(name2code)
      .bundle()
      .pipe(concat())
      .then(bundle_buff => bundle_buff.toString('utf8'))

    for (const name in name2code) {
      if (name2code.hasOwnProperty(name)) {
        bundle.should.include(name2code[name])
      }
    }
  })
})
