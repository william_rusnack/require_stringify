# require_stringify
require js from string while bundling with browserify  #browserify-plugin

Thank you [**Matt DesLauriers** (**mattdesl**)](https://github.com/mattdesl) for helping me solve this.

[![Build Status](https://travis-ci.org/BebeSparkelSparkel/require_stringify.svg?branch=master)](https://travis-ci.org/BebeSparkelSparkel/require_stringify)

## Install Require
```sh
npm install https://github.com/BebeSparkelSparkel/require_stringify.git
```
```javascript
const require_stringify = require('require_stringify')
```

## Load Plugin
```javascript
browserify(file) // create a browserify instance
  .plugin(require_stringify) // load as instance plugin
```

## Require String Modules
After the plugin has been loaded
```javascript
browserify_instance
  .require_stringify(string_files)
```

## string_files Object
string_files is an Object. The keys are the strings that `require` can use to import the value string code.
```javascript
const string_files = {
  module0: 'module.exports = function() {console.log("module0")}',
  module1: 'module.exports = function() {console.log("module1")}',
}
```
To require *module0* from your entry file or anywhere else do:
```javascript
const module0 = require('module0')
```

## Example
```javascript
/*eslint-disable no-console */
/*globals __dirname */

const browserify = require('browserify')
const str = require('string-to-stream')
const concat = require('concat-stream-promise')
const require_stringify = require('require_stringify')

// modules that can be required
const string_files = {
  module0: 'module.exports = function() {console.log("module0")}',
  module1: 'module.exports = function() {console.log("module1")}',
}

// browserify entry stream. Reference: https://github.com/browserify/browserify#browserifyfiles--opts
const entry = str(`
  const module0 = require('module0')
  const module1 = require('module1')
  module0()
  module1()
`)

browserify(entry, {basedir: __dirname})

  // require_stringify part
  .plugin(require_stringify) // add require_stringify to browserify instance
  .require_stringify(string_files) // require your string code

  .bundle()
  .pipe(concat())  // concatenate bundle stream to buffer
  .then(bundle => bundle.toString('utf8'))  // buffer to string

  .then(console.log.bind(console))  // log bundled js
  .catch(console.error.bind(console))

```

## Originates From
Created because of [Browserify Issue #1813](https://github.com/browserify/browserify/issues/1813)
