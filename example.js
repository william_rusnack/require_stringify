/*eslint-disable no-console */
/*globals __dirname */

const browserify = require('browserify')
const str = require('string-to-stream')
const concat = require('concat-stream-promise')
// const require_stringify = require('require_stringify')
const require_stringify = require('./index.js')

// modules that can be required
const string_files = {
  module0: 'module.exports = function() {console.log("module0")}',
  module1: 'module.exports = function() {console.log("module1")}',
}

// browserify entry stream. Reference: https://github.com/browserify/browserify#browserifyfiles--opts
const entry = str(`
  const module0 = require('module0')
  const module1 = require('module1')
  module0()
  module1()
`)

browserify(entry, {basedir: __dirname})

  // require_stringify part
  .plugin(require_stringify) // add require_stringify to browserify instance
  .require_stringify(string_files) // require your string code

  .bundle()
  .pipe(concat())  // concatenate bundle stream to buffer
  .then(bundle => bundle.toString('utf8'))  // buffer to string

  .then(console.log.bind(console))  // log bundled js
  .catch(console.error.bind(console))
