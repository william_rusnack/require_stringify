const str = require('string-to-stream')

module.exports = function (b) {
  b.require_stringify = function (string_files, extension='.js') {
    for (const name in string_files) {
      if (string_files.hasOwnProperty(name)) {
        this
          .exclude(name)
          .require(str(string_files[name]), {
            file: name + extension,
            expose: name,
          })
      }
    }
    return this
  }
}
